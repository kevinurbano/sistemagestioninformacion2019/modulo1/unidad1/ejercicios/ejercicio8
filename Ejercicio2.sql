﻿DROP DATABASE IF EXISTS h8m1u1ej2;
CREATE DATABASE IF NOT EXISTS h8m1u1ej2;
USE h8m1u1ej2;

CREATE OR REPLACE TABLE zona_urbana(
  nombreZona varchar(20),
  categoria varchar(20),
  PRIMARY KEY(nombreZona)
);

CREATE OR REPLACE TABLE casa_particular(
  nombreZona varchar(20),
  numero int,
  m2 float,
  PRIMARY KEY(nombreZona,numero),
  CONSTRAINT fkCasaParticular_zonaUrbana FOREIGN KEY (nombreZona)
    REFERENCES zona_urbana(nombreZona) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE OR REPLACE TABLE bloque_casas(
  calle varchar(20),
  numero int,
  numPisos int,
  nombreZona varchar(20),
  PRIMARY KEY(calle,numero),
  CONSTRAINT fkBloqueCasas_zonaUrbana FOREIGN KEY (nombreZona)
    REFERENCES zona_urbana(nombreZona) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE OR REPLACE TABLE piso(
  calle varchar(40),
  numero int,
  planta int,
  puerta int,
  m2 float,
  PRIMARY KEY (calle,numero,planta,puerta)
);

CREATE OR REPLACE TABLE persona(
  dni varchar(12),
  nombre varchar(30),
  edad int,
  nombreZona varchar(20),
  numeroCasa int,
  calle varchar(40),
  numero int,
  planta int,
  puerta int,
  PRIMARY KEY(dni),
  CONSTRAINT fkPersona_CasaParticular FOREIGN KEY (nombreZona,numeroCasa)
    REFERENCES casa_particular(nombreZona,numero) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fkPersona_Piso FOREIGN KEY (calle,numero,planta,puerta)
    REFERENCES piso(calle,numero,planta,puerta) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE OR REPLACE TABLE poseeC(
  dniPersona varchar(12),
  nombreZona varchar(20),
  numero int,
  PRIMARY KEY(dniPersona,nombreZona,numero),
  CONSTRAINT fkPoseeC_Persona FOREIGN KEY (dniPersona)
    REFERENCES persona(dni) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fkPoseeC_CasaParticular FOREIGN KEY(nombreZona,numero)
    REFERENCES casa_particular(nombreZona,numero) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE OR REPLACE TABLE poseeP(
  dniPersona varchar(12),
  calle varchar(40),
  numero int,
  planta int,
  puerta int,
  PRIMARY KEY(dniPersona,calle,numero,planta,puerta),
  CONSTRAINT fkPoseeP_Persona FOREIGN KEY (dniPersona)
    REFERENCES persona(dni) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fkPoseeP_Piso FOREIGN KEY (calle,numero,planta,puerta)
    REFERENCES piso(calle,numero,planta,puerta) ON DELETE CASCADE ON UPDATE CASCADE
);



