﻿DROP DATABASE IF EXISTS m1u1h8ej3;

CREATE DATABASE IF NOT EXISTS m1u1h8ej3;

USE m1u1h8ej3;

CREATE OR REPLACE TABLE especies(
  nombre varchar(40),
  caracteristica text,
  PRIMARY KEY(nombre)
);

CREATE OR REPLACE TABLE mariposas(
  nombreCientifico varchar(40),
  origen varchar(40),
  habitat varchar(40),
  esperanzaVida varchar(50),
  especie varchar(40),
  PRIMARY KEY(nombreCientifico)
);

CREATE OR REPLACE TABLE mariposasColores(
  mariposa varchar(40),
  color varchar(40),
  PRIMARY KEY(mariposa,color)
);

CREATE OR REPLACE TABLE ejemplares(
  mariposa varchar(40),
  numero int,
  procedencia varchar(40),
  tamanio int,
  coleccion int,
  PRIMARY KEY(mariposa,numero) 
);

CREATE OR REPLACE TABLE colecciones(
  codigo int,
  precioEstimado float,
  ubicacion varchar(50),
  PRIMARY KEY(codigo)
);

CREATE OR REPLACE TABLE ColeccionesEspecies(
  coleccion int,
  especie varchar(40),
  mejorEjemplar varchar(60),
  PRIMARY KEY(coleccion,especie)
);

CREATE OR REPLACE TABLE Persona(
  dni varchar(12),
  nombre varchar(40),
  direccion varchar(50),
  telefono varchar(12),
  coleccion int,
  esPrincipal boolean,
  PRIMARY KEY(dni)
);

/* Faltan restricciones */